
module VR

  class SimpleComboBoxEntry < Gtk::ComboBoxEntry
    def initialize(active_choice, *choices)
      super(true)
      append_text(active_choice)
      choices.each { |c|  append_text(c) if c != active_choice }
      self.active = 0
    end
  end


end
