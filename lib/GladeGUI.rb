# 
#  GladeGUI connects your class to a glade form using 
#  the #load_glade(__FILE__) method.  It will load a .glade
#  file into memory, enabling your ruby programs to have a GUI interface.
#  
#  GladeGUI works by adding an instance variable, @builder to your class.  The @builder
#  variable is an instance of {Gtk::Builder}[http://ruby-gnome2.sourceforge.jp/hiki.cgi?Gtk%3A%3ABuilder]
#  It holds references to all your windows and widgets.
#  
#  ==Include the GladeGUI interface
#  
#  To use the GladeGUI interface, include this line after your 
#  “class” declaration:
#  
#   include GladeGUI
#  
#  Here is an example of a class that uses GladeGUI:
#  
#   class MyClass
#    
#     include GladeGUI
#    
#     def show()
#       load_glade(__FILE__)  #loads file, /glade/MyClass.glade into @builder
#       show_window() 
#     end  
#    
#   end
#  
#  The load_glade() method is part of GladeGUI.  It will load a corresponding 
#  glade file for this class.  It knows which glade file to load by using a naming 
#  convention:
#  
#   /home/me/project/MyClass.rb 
#  
#  Will load this glade file:
#  
#   /home/me/project/glade/MyClass.glade
#  
#  Note: it will use this format: /my/path/glade/<class_name>.glade.
#  In the example, the class and the file have the same name,
#  MyClass.  You should always name your class, script, and glade file
#  the same name (case sensitive).
#  
#  ==@builder holds all your widgets
#  
#  So when you "load" your class's glade file where is it loaded?
#  
#  GladeGUI adds an instance variable, @builder to your class.
#  It loads all the windows and widgets from your glade file into @builder.
#  So, you use @builder to manipulate everything in you class's GUI.
#  @builder is set when you call the load_glade() method, as this code shows:
#  
#   class MyClass
#    
#     include GladeGUI
#    
#     def show()
#       puts @builder.to_s   #  => nil
#       load_glade(__FILE__)  #loads file, /glade/MyClass.glade into @builder
#       puts @builder.to_s  #  =>  Gtk::Builder
#       show_window() 
#     end  
#    
#   end
#  
#  After load_glade() is called, you can access any of your form's windows or widgets
#  using the @builder variable: 
#  
#   @builder["window1"].title = "This is the title that appears at the top."
# 
#  You use the @builder variable to access everything on your form.  In the above example,
#  @builder["window1"] is a reference to the main Gtk::Window of your class.
#  Gtk::Window#title sets the title that appears at the top of the
#  window, So, this will set that property.  
#  
#  Here's another example:  Suppose you have a glade form with a Gtk::Entry box on it named "email."
#  You could set the text that appears in the Gtk::Entry by setting the Gtk::Entry#text property:
#  
#   @builder["email"].text = "harvey@harveyserver.com"
#  
#  Now the email adddess is set with a new value:
# 
#  http://visualruby.net/img/gladegui_simple.jpg
#  
#  ==Auto fill your glade form
#  You can streamline the process of setting-up your forms by
#  auto-filling the widgets from instance variables with the same name.
#
#  When assigning names to widgets in glade, give them names that 
#  correspond to your instance variables. For example, if you want 
#  to edit an email address on the glade form, create an instance 
#  variable named @email in your class. Then, in glade, you 
#  add a Gtk::Entry widget to your form and set its name to 
#  “email”. The advantage of this is that GladeGUI will populate 
#  the “email” widget in glade using the @email variable. so 
#  you don’t need to include the above line of code. (see 
#  set_glade_variables() method.)


module GladeGUI

  attr_accessor :builder

  def set_parent(parent) # :nodoc: parent = class derived from class GladeGUI
      @builder["window1"].transient_for = parent.builder["window1"]  
  end

##
#  This will Load the glade form.  
#  It will create a Gtk::Builder object from your glade file.
#  The Gtk::Builder object is stored in the instance variable, @builder.
#  You can get a reference to any of the widgets on the glade form by
#  using the @builder object:
#  
#    widget = @builder["name"]
#  
#  Normally, you should give your widgets names of instance variables: i.e. "MyClass.var"
#  so they can be autoloaded using the set_glade_all() method.
#  
#  You can also pass a reference to a parent class that also includes GladeGUI.
#  Then the child window will run simultaniously with the parent.
#  

  def load_glade(caller__FILE__, parent = nil)
    file_name = File.split(caller__FILE__)[0] + '/glade/' + class_name(self) + ".glade"   
    @builder = Gtk::Builder.new.add_from_file(file_name)
    @builder.connect_signals{ |handle| method(handle) }
    set_parent(parent) unless parent.nil?
    parse_signals()
  end

  def class_name(obj) # :nodoc:
    /.*\b(\w+)$/.match(obj.class.name)[1]
  end    

  def parse_signals() # :nodoc:
    meths = self.class.instance_methods()
    meths.each do |meth|
      meth = meth.to_s #bug fix ruby 1.9 gives stmbol
      glade_name, signal_name = *(meth.split("__"))
      next if (signal_name.to_s == "" or glade_name.to_s == "") #covers nil
      @builder.objects.each do |obj|
        next unless obj.respond_to?(:builder_name)
        if obj.builder_name == glade_name or obj.builder_name =~ /^(?:#{class_name(self)}\.|)#{glade_name}\[\d+\]$/ #arrays
          obj.signal_connect(signal_name) { |*args| method(meth.to_sym).call(*args) } 
        end
      end
      obj = glade_name == "self" ? self : instance_variable_get("@" + glade_name)
      obj ||= eval(glade_name) if respond_to?(glade_name) and method(glade_name.to_sym).arity == 0 # no arguments!
      obj.signal_connect(signal_name) { |*args| method(meth.to_sym).call(*args) } if obj.respond_to?("signal_connect") 
    end
  end

#  This method is the most useful method to populate a glade form.  It will
#  populate from active_record fields and instance variables.  It will simply 
#  call both of these methods:
#  
#   set_glade_active_record()
#   set_glade_variables()
#  
#  So, to set all the values of a form, simply call the set_glade_all() method instead.

  def set_glade_all(obj = self) 
     set_glade_active_record(obj)
    set_glade_variables(obj)
  end

#  This method is the most useful method to retreive values from a glade form.  It will
#  populate from active_record fields and instance variables.  It will simply 
#  call both of these methods:
#  
#   get_glade_active_record()
#   get_glade_variables()
#  
#  So, to retreive all the values of a form back into your ActiveRecord object and instance variables, simply call the set_glade_all() method instead.

  def get_glade_all(obj = self)
     get_glade_active_record(obj)
    get_glade_variables(obj)
  end

#  Populates the glade form from the fields of an ActiveRecord object.
#  So instead of having to assign each widget a value:
#  
#      @builder["ARObject.name"].text = @name
#      @builder["ARObject.address"].text = @address
#      @builder["ARObject.email"].text = @eamil
#      @builder["ARObject.phone"].text = @phone
#  
#  you can write one line of code:
#  
#    set_glade_active_record()
#  
#  The optional parameter is seldom used because you usually want the
#  glade form to populate from the calling class.  If you passed another object,
#  the form would populate from it.
#  
#  obj - type ActiveRecord::Base
#  

  def set_glade_active_record(obj = self) 
    return if not defined? @attributes
    obj.attributes.each_pair { |key, val| fill_control(class_name(obj) + "." + key, val) }
  end

#Populates the glade form from the instance variables of the class.
#This works for Gtk:Button, Gtk::Entry, Gtk::Label and Gtk::Checkbutton.
#So instead of having to assign each widget a value:
#
#    @builder["DataObjectGUI.name"].text = @name
#    @builder["DataObjectGUI.address"].text = @address
#    @builder["DataObjectGUI.email"].text = @eamil
#    @builder["DataObjectGUI.phone"].text = @phone
#
#you can write one line of code:
#
#  set_glade_variables()
#
#The optional parameter is seldom used because you usually want the
#glade form to populate from the calling class.  If you passed another object,
#the form would populate from it.
#
#obj - type Object
#

  def set_glade_variables(obj = self) 
    obj.instance_variables.each do |name|
      name = name.to_s #ruby 1.9 passes symbol!
      v = obj.instance_variable_get(name)
      if v.class == Array 
        (0..v.size-1).each do |i|
          fill_control(class_name(obj) + "." + name.gsub("@", "") + "[" + i.to_s + "]", v[i] )
        end  
      else
        fill_control(class_name(obj) + "." + name.gsub("@", ""), v)
      end
    end
  end

  def fill_control(glade_name, val) # :nodoc:
    control = @builder[glade_name]
    control ||= @builder[glade_name.split(".")[1]]
    case control
      when Gtk::CheckButton then control.active = val
      when Gtk::TextView then control.buffer.text = val.to_s
      when Gtk::Entry then control.text = val.to_s
      when Gtk::FontButton then control.font_name = val.to_s
      when Gtk::LinkButton then control.uri = control.label = val.to_s 
      when Gtk::Label, Gtk::Button then control.label = val.to_s
      when Gtk::Image then control.file = val.to_s 
      when Gtk::SpinButton then control.value = val.to_f
      when Gtk::ProgressBar then control.fraction = val.to_f
      when Gtk::Calendar then control.select_month(val.month, val.year) ; control.select_day(val.day) ; control.mark_day(val.day)
      when Gtk::Adjustment then control.value = val.to_f
    end        
  end

#  Populates the instance variables from the glade form.
#  This works for Gtk:Button, Gtk::Entry, Gtk::Label and Gtk::Checkbutton.
#  So instead of having to assign instance variable:
#  
#    @name = @builder["DataObjectGUI.name"].text 
#    @address = @builder["DataObjectGUI.address"].text 
#    @eamil = @builder["DataObjectGUI.email"].text 
#    @phone = @builder["DataObjectGUI.phone"].text 
#    
#  you can write one line of code:
#    
#    get_glade_variables()
#    
#  The optional parameter is seldom used because you usually want the
#  glade form to populate from the calling class.  If you passed another object,
#  the form would populate from it.
#  
#  obj - type Object
#

  def get_glade_variables(obj = self)
    obj.instance_variables.each do |v|
      v = v.to_s  #fix for ruby 1.9 giving symbols
      control = @builder[class_name(obj) + v.gsub("@", ".")]
      control ||= @builder[v.gsub("@", "")]
      case control
        when Gtk::CheckButton then obj.instance_variable_set(v, control.active?)
        when Gtk::Entry then obj.instance_variable_set(v, control.text)
        when Gtk::TextView then obj.instance_variable_set(v, control.buffer.text)
        when Gtk::FontButton then obj.instance_variable_set(v, control.font_name) 
        when Gtk::Label, Gtk::Button then obj.instance_variable_set(v, control.label)
        when Gtk::SpinButton then obj.instance_variable_set(v, control.value)
        when Gtk::Image then obj.instance_variable_set(v, control.file)
        when Gtk::ProgressBar then obj.instance_variable_set(v, control.fraction)
        when Gtk::Calendar then obj.instance_variable_set(v, DateTime.new(*control.date))
        when Gtk::Adjustment then obj.instance_variable_set(v, control.value)
      end        
    end
  end


  def get_glade_active_record(obj) # :nodoc:
    return if not defined? @attributes
    obj.attributes.each_pair do |key, val|
      control = @builder[class_name(obj) + "." + key]
      control ||= @builder[key]
      case control
        when Gtk::CheckButton then self.send("#{key}=", control.active?)
        when Gtk::Entry then self.send("#{key}=", control.text)
        when Gtk::TextView then self.send("#{key}=", control.buffer.text)
        when Gtk::FontButton then self.send("#{key}=", control.font_name) 
        when Gtk::Label, Gtk::Button then self.send("#{key}=", control.label)
        when Gtk::SpinButton then self.send("#{key}=", control.value)
        when Gtk::Image then self.send("#{key}=", control.file)
        when Gtk::ProgressBar then self.send("#{key}=", control.fraction)
        when Gtk::Calendar then self.send("#{key}=", DateTime.new(*control.date))
        when Gtk::Adjustment then self.send("#{key}=", control.value)
      end   
    end
  end




#
#This shows the window and will start the Gtk.main loop (for top level windows.)
#This is called after calling load_glade() and setting-up all the form widgets.
#Using show_window() and destroy_window() is better than using Gtk.main and
#Gtk.main.quit because they automatically open and close windows properly.
#
  def show_window()
    @builder["window1"].show_all
    Gtk.main if @builder["window1"].transient_for.nil?  
  end  
#
#Destroys window that was opened with show_window().  destroy_window()
#will check if your window is a parent window or a child, and destroy 
#the window in the proper way.  (It will call Gtk.main.quit for
#parent windows.)
#
  def destroy_window()
    if @builder["window1"].transient_for.nil? 
      Gtk.main_quit 
    end
    @builder["window1"].destroy
  end

  def active_record_valid?(show_errors = true)
    get_glade_all
    if not self.valid?
      VR.msg(self.errors.full_messages.join("\n\n")) if show_errors
      self.reload
      return false
    end
    return true
  end

end
  
  
