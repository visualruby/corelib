module Gtk

  class Widget
  
    attr_accessor :dragged_widget
  
    def drag_array(element = nil)
      @dnd ||= [] 
      @dnd.push(element) if not element.nil?
      return @dnd
    end
  

    def drag_to(target_widget = self, button = Gdk::Window::BUTTON1_MASK)

      @target_widgets ||= []
      @target_widgets.push(target_widget)

      target = Gtk::Drag::TARGET_SAME_APP #useless
      action = Gdk::DragContext::ACTION_MOVE #useless
      dest =  Gtk::Drag::DEST_DEFAULT_ALL #useless

      Gtk::Drag.source_set(self, button, [ [self.object_id.to_s, target, 0] ],  action)  
      ar = target_widget.drag_array([ self.object_id.to_s, target, 0])
      Gtk::Drag.dest_set(target_widget, dest , ar , action) 
      if not @done
        @done = true
        self.signal_connect("drag_begin") do |widget, context|
          @target_widgets.each { |widg| widg.dragged_widget = self } 
        end        
      end
  
    end
  
  end

end
