module VR
  class Thread
    def self.new(*args,&block)
      ::Thread.new(*args,&block)
    end

    def self.init
      @@mutex = Mutex.new
      @@pending_calls = []
      @@running = true
      GLib::Timeout.add(100) do
        @@mutex.synchronize do
          @@pending_calls.each do |closuer|
            closuer.call
          end
          @@pending_calls = []
        end
        @@running
      end
    end
    
    def self.destroy
      @@running = false
    end
    
    def self.protect(&block)
      if ::Thread.current == ::Thread.main
        block.call
      else
        @@mutex.synchronize do
          @@pending_calls << block
        end
      end
    end
    
    def self.protect_call(obj,sym,*args)
      if ::Thread.current == ::Thread.main
        obj.__send__(sym,*args)
      else
        @@mutex.synchronize do
          @@pending_calls << Proc.new do
            obj.__send__(sym,*args)
          end
        end
      end
    end
    
    def self.flush
      if @@mutex.try_lock
        @@pending_calls.each do |closuer|
          closuer.call
        end
        
        @@pending_calls = []
        @@mutex.unlock
      end
    end
  end
end