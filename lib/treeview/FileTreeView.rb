
module VR

class FileTreeView < TreeView # :nodoc:

  unless defined? FILETREEVIEW_GTYPE
    FILETREEVIEW_GTYPE = self.name.split(":").collect {|x| x.empty? ? nil : x}.compact.join("_")
    type_register(FILETREEVIEW_GTYPE)
  end

  def initialize(icon_path = nil, width = nil, height = nil) #, &block)
    super(:file => {:pix => Gdk::Pixbuf, :file_name => String}, :path => String, :modified_date => VR::DateCol, :sort_on => String)
    col_visible( :path => false, :modified_date => false, :sort_on => false)
    self.headers_visible = false
    model.set_sort_column_id(id(:sort_on))
    @icons = File.directory?(icon_path.to_s) ? VR::IconHash.new(icon_path,width,height) : nil
  end

  def get_open_folders()
    expanded = []
    map_expanded_rows {|view, path| expanded << model.get_iter(path)[id(:path)] }
    return expanded
  end

  def open_folders(folder_paths)
    collapse_all
    model.each do |model, path, iter| 
      if folder_paths.include?(iter[id(:path)])   
        expand_row(path, false) 
      end
    end
  end  

  def refresh(root = Dir.pwd, glob = File.join(root, "**","*"))
    @root_dir = root
    expanded_folders = get_open_folders()
    self.model.clear
    add_file(root, nil)
    Dir.glob(glob).each do |fn|
       insert(fn)
    end
    open_folders(expanded_folders)
    self.expand_row(self.model.iter_first.path, false)
  end

  def insert(fn)  #fn is absolute path
    fn.gsub!("\\", "/")
    return if not File.exists?(fn) or get_iter_from_path(fn) or not fn.match(/^#{model.iter_first[id(:path)]}/)
    if not parent_iter = get_iter_from_path(File.dirname(fn))
      parent_iter = insert(File.dirname(fn))
    end
    return add_file(fn, parent_iter)
  end
  
  def get_iter_from_path(fn)
    model.each do | model, path, iter|
      if iter[id(:path)] == fn 
        return iter 
      end
    end 
    return false  
  end

  def add_file(fn, parent)
      fn.gsub!("\\", "/")
      child = model.append(parent)
      if @root_dir == fn && @icons.get_icon("x.project") != @icons.get_icon("unknown")
        child[id(:pix)] = @icons.get_icon("x.project")
      else
        child[id(:pix)] = @icons.get_icon(File.directory?(fn) ? "x.folder" : fn)
      end
      child[id(:file_name)] = File.basename(fn)
      child[id(:path)] = fn
      child[id(:sort_on)] = (File.directory?(fn) ? "0" : "1") + child[id(:file_name)]
      return child
  end
  

  def folder?(iter) iter[id(:sort_on)][0,1] == "0" end
  def file_name(iter) iter ? iter[id(:path)] : nil end
  def get_selected_file_name() 
    selection.selected ? selection.selected[id(:file_name)] : nil
  end
  def get_selected_path() (selection.selected ? selection.selected[id(:path)] : nil) end
  def delete_selected()  self.model.remove(selection.selected) if selection.selected end

end  

end


