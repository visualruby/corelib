module VR 

  class TreeViewColumn < Gtk::TreeViewColumn
  
    def initialize(view, model_col, sym, type)
        super()
        self.title = sym.to_s.gsub("_", " ").split(' ').map {|w| w.capitalize }.join(' ') # My Title
        @view = view
        self.resizable = true
        cols = (type.is_a? Hash) ? type : {sym => type}
        cols.each_pair do |symb, type|
          a = [model_col, self, @view, symb]
          if type == Gdk::Pixbuf
            ren = VR::CellRendererPixbuf.new(*a)
            self.pack_start( ren, false )
            self.add_attribute( ren, :pixbuf,  model_col)
          elsif type == TrueClass
            ren = VR::CellRendererToggle.new(*a)
            self.pack_start( ren, false )
            self.add_attribute( ren, :active,  model_col)
          elsif type == VR::SpinCol
            ren = VR::CellRendererSpin.new(*a)
            self.pack_start( ren, false )
            self.add_attribute( ren, :adjustment,  model_col)
            self.set_cell_data_func(ren) do |col, renderer, model, iter|
              fmt = "%.#{renderer.digits}f"
              renderer.text = fmt % iter[renderer.model_col].value    
            end  
          elsif type == VR::ComboCol 
            r = VR::CellRendererCombo.new(*a)
            self.pack_start( r, false )
            self.set_cell_data_func(r) do |col, ren, model, iter|
              ren.text = iter[ren.model_col].selected.to_s
            end 
          elsif type == VR::ProgressCol 
            r = VR::CellRendererProgress.new(*a)
            self.pack_start( r, false )
            self.add_attribute( r, :value,  model_col)
          elsif type == DateTime 
            r = VR::CellRendererDate.new(*a)
            self.pack_start( r, false )
            self.set_cell_data_func(r) do |col, ren, model, iter|
              ren.text = iter[ren.model_col].strftime(ren.date_format)
            end
          elsif type == String or type == Float or type == Integer or type == Fixnum  
            r = VR::CellRendererText.new(*a)
            self.pack_start( r, false )
            self.add_attribute( r, :text,  model_col)
          else #user defined object
            r = VR::CellRendererObject.new(*a)
            self.pack_start( r, false )
            self.set_cell_data_func(r) do |col, ren, model, iter|
              ren.render_object(iter)
            end
          end
          model_col = model_col + 1
        end
    end
  
    def width=(w) #pixels
      self.sizing = Gtk::TreeViewColumn::FIXED
      self.fixed_width = w
    end
 
    def sortable=(is_sortable) 
      self.sort_column_id = cell_renderers[0].model_col
      self.clickable = is_sortable 
    end

  end

end
#class VR::TreeViewColumn < Gtk::TreeViewColumn

#  TYPES = { 
#    Gdk::Pixbuf => VR::CellRendererPixbuf,
#    TrueClass => VR::CellRendererToggle,
#    Gtk::Adjustment => VR::CellRendererSpin,
#    VR::Combo => VR::CellRendererCombo,
#    VR::Progress => VR::CellRendererProgress
#  }
#  
#  ATTR = {
#    VR::CellRendererPixbuf => "pixbuf",
#    VR::CellRendererToggle => "active",
#    VR::CellRendererSpin => "adjustment",
#    VR::CellRendererText => "text",
#    VR::CellRendererProgress => "value"
#  }


#  def initialize(view, model_col, *args)
#    super()
#    self.resizable = true
#    args = args.flatten
#    args.each do |type|    
#      t = TYPES.key?(type) ? TYPES[type] : VR::CellRendererText
#      ren = t.new(model_col, view)
#      self.pack_start( ren, false )
#      self.add_attribute( ren, ATTR[t],  model_col) if ATTR.key?(t)
#      model_col = model_col + 1
#      if t.is_a VR::Cell
#    end
#  end
#end


#end
