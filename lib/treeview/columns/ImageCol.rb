
module VR

  class ImageCol #< Gdk::Pixbuf

    include GladeGUI
 
    def initialize(image)
#      super(File.dirname(__FILE__) + "/../../../img/image-x-generic.png")
      @image1 = image
    end

    def show()
      load_glade(__FILE__)      
      set_glade_variables()
      @builder["window1"].resize 500, 360
      show_window()
    end

    def to_s
      "<Img>"
    end

#    def visual_attributes
#      { :pixbuf => self }
#    end

  end

end
