module VR

  def VR.msg(*a)  Dialog.message_box(*a) end

  module Dialog
    
#    def Dialog.calendar(datetime)
#        dialog = Gtk::Dialog.new(
#          "Select Date", nil,
#          Gtk::Dialog::MODAL,
#          [Gtk::Stock::OK, Gtk::Dialog::RESPONSE_ACCEPT],
#          [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_REJECT])
#        c = Gtk::Calendar.new
#        c.select_month(datetime.month, datetime.year)
#        c.select_day(datetime.day)
#        c.mark_day(datetime.day)        
#        dialog.vbox.add(c)
#        dialog.show_all
#        ret = dialog.run == Gtk::Dialog::RESPONSE_ACCEPT ? DateTime.new(*c.date) : nil
#         dialog.destroy
#        return ret
#    end
  
    def Dialog.message_box(message, title = "Visual Ruby")
      dialog = Gtk::MessageDialog.new(
        nil,
        Gtk::Dialog::MODAL,
        Gtk::MessageDialog::INFO,
        Gtk::MessageDialog::BUTTONS_OK,
        message)
      dialog.title = title
      dialog.show_all
      dialog.run
      dialog.destroy
    end  
    
    def Dialog.input_box(message, default="", title = "Visual Ruby")
        dialog = Gtk::MessageDialog.new(
          nil,
          Gtk::Dialog::MODAL,
          Gtk::MessageDialog::QUESTION,
          Gtk::MessageDialog::BUTTONS_OK_CANCEL,
          message)
        dialog.title = title
        input = Gtk::Entry.new
        dialog.vbox.add(input)
        dialog.show_all
        ret = ""
        dialog.run do |response|
          if response == Gtk::Dialog::RESPONSE_OK
            ret = input.buffer.text
          else
            ret = false
          end
        end
         dialog.destroy
        return ret
    end
  
    def Dialog.ok_box(message, title = "Visual Ruby")
        dialog = Gtk::MessageDialog.new(
          nil,
          Gtk::Dialog::MODAL,
          Gtk::MessageDialog::QUESTION,
          Gtk::MessageDialog::BUTTONS_OK_CANCEL,
          message)
        dialog.title = title
        ret = (dialog.run == Gtk::Dialog::RESPONSE_OK)
         dialog.destroy
        return ret
    end
  
    def Dialog.folder_box(builder)
      dialog = Gtk::FileChooserDialog.new("Select Folder...",
                     builder['window1'],
                     Gtk::FileChooser::ACTION_SELECT_FOLDER,
                     nil,
                     [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_CANCEL],
                     [Gtk::Stock::OPEN, Gtk::Dialog::RESPONSE_ACCEPT])
  
  #    if pattern != nil
  #      filter = Gtk::FileFilter.new
  #      filter.add_pattern(pattern)
  #      filter.name = "VR Project Folders"   
  #      dialog.add_filter(filter)
  #    end
      if dialog.run == Gtk::Dialog::RESPONSE_ACCEPT
        ret =  dialog.current_folder
      else
        ret = false
      end 
      dialog.destroy
      return ret
    end
  end

  def Dialog.yesno(message, title = "Visual Ruby")
    dialog = Gtk::MessageDialog.new(
      nil,
      Gtk::Dialog::MODAL,
      Gtk::MessageDialog::QUESTION,
      Gtk::MessageDialog::BUTTONS_YES_NO,
      message)
    dialog.title = title
    ret = (dialog.run == Gtk::Dialog::RESPONSE_YES)
    dialog.destroy
    return ret
  end
  
  def Dialog.userpass(message, title = "Visual Ruby")
    dialog = Gtk::Dialog.new(
      title,
      nil,
      Gtk::Dialog::MODAL,
      [Gtk::Stock::OK,Gtk::Dialog::RESPONSE_ACCEPT],
      [Gtk::Stock::CANCEL, Gtk::Dialog::RESPONSE_REJECT])
    dialog.vbox.add(Gtk::Label.new(message))
    hbox = Gtk::HBox.new
    hbox.add(Gtk::Label.new("Username:"))
    hbox.add(user = Gtk::Entry.new)
    dialog.vbox.add(hbox)
    hbox = Gtk::HBox.new
    hbox.add(Gtk::Label.new("Password:"))
    hbox.add(pass = Gtk::Entry.new)
    dialog.vbox.add(hbox)
    pass.visibility = false
    ret = (dialog.run == Gtk::Dialog::RESPONSE_ACCEPT) ? {:user => user.text, :pass => pass.text } : nil
    dialog.destroy
    return ret
  end
  
  def Dialog.listbox(message,list,title="Visual Ruby",icon=Gtk::Stock::DIALOG_QUESTION)
    dialog = Gtk::Dialog.new(
      title,
      nil,
      Gtk::Dialog::MODAL,
      [Gtk::Stock::YES, Gtk::Dialog::RESPONSE_ACCEPT],
      [Gtk::Stock::NO,Gtk::Dialog::RESPONSE_REJECT])
    hbox = Gtk::HBox.new
    hbox.add(Gtk::Image.new(icon,Gtk::IconSize::DIALOG))
    hbox.add(Gtk::Label.new(message))
    dialog.vbox.add(hbox)
    vrlist = VR::ListView.new({:item=>String})
    list.each do |entry|
      vrlist.add_row(:item => entry)
    end
    vrlist.headers_visible = false
    holder = Gtk::ScrolledWindow.new
    holder.add(vrlist)
    dialog.vbox.add(holder)
    dialog.vbox.show_all
    ret = (dialog.run == Gtk::Dialog::RESPONSE_ACCEPT)
    dialog.destroy
    return ret
  end

end  
