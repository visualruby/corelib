module VR

  class IconHash < Hash # :nodoc:
  
    def initialize(path,width=nil,height=nil)
      Dir.glob(path + "/*.png").each do |f|
        ext = File.basename(f, ".png")
        if width.nil?
          self[ext] = Gdk::Pixbuf.new(f)
        else
          self[ext] = Gdk::Pixbuf.new(f,width,height)
        end
      end
    end
  
    def get_icon(file_name)
      ext = File.extname(file_name).gsub(".", "")
      self.has_key?(ext) ? self[ext] : self["unknown"] 
    end
  end

end
