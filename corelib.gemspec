# -*- encoding: utf-8 -*-

Gem::Specification.new do |s|
  s.name = "vr-corelib"
  s.version = "0.0.36"
  s.platform = Gem::Platform::RUBY
  s.authors = ["Mario Steele","Eric Cunningham"]
  s.email = ["mario@ruby-im.net","eric@visualruby.net"]
  s.homepage = "http://www.visualruby.net/"
  s.summary = "Library to make GUIs with Ruby"
  s.description = "Library to make GUIs with Ruby.  This library is a dependency of visualruby.  This library is useful in the context of visualruby.  Go to visualruby.net to download visualruby."
  
  s.add_dependency "require_all", ">=1.2.1"
  s.add_dependency "gtk2", ">=1.1.2"
  s.files = Dir.glob("{doc,glade,img,lib}/**/*") + %w(Demo.rb main.rb vr-corelib.rb vrlib.rb)
  s.require_path = '.'
end
